from dataclasses import field, fields
from pyexpat import model
from socket import fromshare
from django import forms
from .models import List

class ListForm(forms.ModelForm):
    class Meta:
        model = List
        fields = ["item", "completed"]